# Magento 2 Vietnamese Language Pack (vi_VN) v1.0.0
## Installation
Add repository to project composer.json file:
```
{
      "type": "vcs",
      "url": "git@bitbucket.org:dtnsolutions/magento2-language-vi-vn.git",
      "no-api": true
}
```
Run these commands in document root:
```
composer require dtn/magento2-language-vi-vn:dev-master
php bin/magento setup:upgrade
```
## Configuration
Go to Stores > Settings > Configuration > General > General > Locale Options > Locale and change the locale to **Vietnamese (Vietnam)**

This will change the display language on store front.

To change the language for the admin interface, click to your admin account name on the top right of the admin panel and select **Account Setting**. In the Account Information screen, change the Interface Locale to Vietnamese (Vietnam) and click **Save Account**.